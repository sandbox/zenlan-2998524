function initMobilenetJS(properties) {
  new MJS_example(properties);
}

class MJS_example {

  constructor(properties) {
    this.state = { loading: false };
    this.properties = properties;
    this.ui = new MJSUI_example({
//      onFoo: this.foo.bind(this),
      onHeaders: this.fetchHeaders.bind(this),
      onImage: this.fetchImage.bind(this),
      onPredict: this.fetchPredictions.bind(this)
    });
  }

  async foo() {
    let predictions = [
      {className : 'foo, bar', probability : 0.66666666},
      {className : 'la, di, da', probability : 0.33333333},
      {className : 'nothing', probability : 0.001},
    ];
    const html = await this.postPredictions(predictions);
    this.ui.showCaption(html);
  }

  async postPredictions(data) {
    try {
      const res = await fetch(this.properties.urlPost, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let html = '';
      console.log('postPredictions');
      console.log(res);
      if (res.status >= 400) {
        return 'not found';
      }
      const json = await res.json();
      json.forEach(command => {
        console.log('Command:', command);
        if (command.selector == 'figure') {
          html = command.data;
        }
      });
      return html;
    } catch (err) {
      console.log('postPredictions', err);
      return err;
    }
  }

  async setLoading(state) {
    this.state.loading = state;
    this.ui.showStatus(state ? 'loading' : '');
  }

  async fetchHeaders(endpoint, options) {
    if (this.state.loading) return;
    await this.setLoading(true);
    let self = this;
    options = options ? options : {};
    options.method = 'HEAD';
    return await fetch(endpoint, options)
      .then(res => {
        console.log('fetchHeaders')
        console.log(res);
        if (res.status >= 400) {
          return false;
        }
        let headers = [];
        res.headers.forEach(function(val, key) {
          headers.push(key + ' -> ' + val);
        });
        this.ui.showHeaders(headers);
        this.setLoading(false);
        return true;
      });
  }

  async fetchImage(endpoint, options) {
    if (this.state.loading) return;
    console.log('fetchImage begin');
    await this.setLoading(true);
    options = options ? options : {};
    let img = false;
    try {
      const res = await this.fetchRes(endpoint, options);
      if (!res) {
        throw new Error('Fetch failed');
      }
      const blob = await this.getBlob(res);
      if (!blob) {
        throw new Error('Could not get binary object');
      }
      img = await this.loadBlob(blob);
      if (!img) {
        throw new Error('Could not load binary object');
      }
    } catch (err) {
      console.log('fetchImage', err);
      this.ui.error(err);
    } finally {
      console.log('fetchImage final');
      await this.setLoading(false);
      return img;
    }
  }

  async fetchPredictions(endpoint, options) {
    if (this.state.loading) return;
    console.log('fetchPredictions begin');
    await this.setLoading(true);
    try {
      await this.ui.showStatus('loading image please wait');
      let img = await this.loadImage();
      if (!img) {
        options = options ? options : {};
        this.state.loading = false;
        img = await this.fetchImage(endpoint, options);
        this.state.loading = true;
        if (!img) {
          img = await this.loadImage();
        }
      }
      if (!img) {
        throw new Error('Image load failed');
      }
      if (!this.model) {
        await this.ui.showStatus('loading model please wait');
        this.model = await window.mobilenet.load();
        await this.ui.showStatus('model loaded');
      }
      if (!this.model) {
        throw new Error('Model load failed');
      }
      await this.ui.showStatus('getting predictions please wait');
      const predictions = await this.model.classify(img);
      if (!predictions) {
        throw new Error('Predictions failed');
      }
      console.log(predictions);
      const html = await this.postPredictions(predictions);
      this.ui.showCaption(html);
    } catch (err) {
      console.log('fetchPredictions', err);
      this.ui.error(err);
    } finally {
      await this.setLoading(false);
      console.log('fetchPredictions final');
    }
  }

  async fetchRes(endpoint, options) {
    options = options ? options : {};
    try {
      const res = await fetch(endpoint, options);
      console.log('fetchRes');
      console.log(res);
      if (res.status >= 400) {
        return false;
      }
      return res;
    } catch (err) {
      console.log('fetchRes', err);
      return false;
    }
  }

  async getBlob(res) {
    try {
      const blob = await res.blob();
      console.log('getBlob');
      console.log(blob);
      if (blob.type !== 'text/html') {
        return blob;
      } else {
        return false;
      }
    } catch (err) {
      console.log('fetchRes', err);
      return false;
    }
  }

  async loadBlob(blob) {
    const self = this;
    try {
      const img = await self.ui.loadImage(blob);
      console.log('loadBlob');
      console.log(img);
      if (img) {
        return img;
      } else {
        return false;
      }
    } catch (err) {
      console.log('loadBlob', err);
      return false;
    }
  }

  async loadImage() {
    const self = this;
    try {
      const img = await self.ui.getImage();
      console.log('loadImage');
      console.log(img);
      if (img) {
        return img;
      } else {
        return false;
      }
    } catch (err) {
      console.log('loadImage', err);
      return false;
    }
  }

}

class MJSUI_example {

  constructor(properties) {
    this.properties = properties;

    this.figure = document.querySelector('figure');
    this.info = document.querySelector('.info');
    this.url = document.querySelector('#url');
    this.loadImage = this.loadImage.bind(this);

    this.btnH = document.querySelector('#btn-headers');
    this._onHeaders = this._onHeaders.bind(this);
    this.btnH.addEventListener('click', this._onHeaders);

    this.btnI = document.querySelector('#btn-image');
    this._onImage = this._onImage.bind(this);
    this.btnI.addEventListener('click', this._onImage);

    this.btnP = document.querySelector('#btn-predict');
    this._onPredict = this._onPredict.bind(this);
    this.btnP.addEventListener('click', this._onPredict);

//    this.btnF = document.querySelector('#btn-foo');
//    this._onFoo = this._onFoo.bind(this);
//    this.btnF.addEventListener('click', this._onFoo);

    this.samples = [...document.querySelectorAll('.samples img')];
    this.samples.forEach(sample => sample.addEventListener('click', this._onImgClick));
  }

  _onFoo(e) {
    e.preventDefault();
    this.properties.onFoo();
  }

  _onHeaders(e) {
    e.preventDefault();
    if (document.querySelector('#url').value) {
      this.properties.onHeaders(this.url.value);
    } else {
      alert('Empty URL');
    }
  }

  _onImage(e) {
    e.preventDefault();
    if (document.querySelector('#url').value) {
      this.properties.onImage(this.url.value);
    } else {
      alert('Empty URL');
    }
  }

  _onPredict(e) {
    e.preventDefault();
    if (document.querySelector('#url').value) {
      this.properties.onPredict(this.url.value);
    } else {
      alert('Empty URL');
    }
  }

  _onImgClick(e) {
    if (document.querySelector('#url').value != e.currentTarget.src) {
      document.querySelector('#url').value = e.currentTarget.src;
      document.querySelector('figure').innerHTML = '';
      document.querySelector('.info').innerHTML = '';
    }
  }

  _createImg(blob) {
    const img = document.createElement('img');
    img.src = URL.createObjectURL(blob);
    return new Promise(resolve => {
      img.onload = () => resolve(img)
    })
  }

  async showStatus(message) {
    if (message) {
      this.info.innerHTML = message;
    } else {
      this.info.innerHTML = '';
    }
    return;
  }

  showHeaders(headers) {
    this.figure.innerHTML = headers.join('<br />');
  }

  reset(topic) {
    this.figure.innerHTML = '';
    this.info.innerHTML = '';
  }

  error(message) {
    this.info.innerHTML = message;
  }

  async loadImage(blob) {
    this.img = await this._createImg(blob);
    this.figure.innerHTML = '';
    this.figure.appendChild(this.img);
    return this.img;
  }

  showCaption(caption) {
    const elem = document.createElement('figcaption');
    elem.className = 'figure-caption';
    elem.innerHTML = caption;
    this.figure.appendChild(elem);
  }

  getImage() {
    return this.figure.querySelector('img');
  }

}

