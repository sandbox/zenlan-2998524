<?php

namespace Drupal\mobilenetjs_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Url;

/**
 * Class MobilenetJsExampleController.
 */
class MobilenetJsExampleController extends ControllerBase {

  /**
   * Home.
   *
   * @return array
   *   Return render array.
   */
  public function home() {
    $url = URL::fromUri('internal:/' . drupal_get_path('module', 'mobilenetjs_example'));
    return [
      '#theme' => 'mobilenetjs_example',
      'variables' => [],
      '#imgpath' => $url->toString(),
      '#attached' => [
        'library' => [
          'core/drupal.ajax',
          'mobilenetjs/mobilenetjs',
          'mobilenetjs/tensorflow',
          'mobilenetjs/tensorflow.mobilenet',
          'mobilenetjs_example/mobilenetjs_example',
        ],
        'drupalSettings' => [
          'mobilenetjs' => [
            'pathConnect' => Url::fromRoute('mobilenetjs_example.ajax.predictions.html', [])->toString(),
          ],
        ],
      ],
    ];
  }

  /**
   * Respond to an AJAX POST request containing JSON content.
   *
   * @param string $nojs
   *   String ='ajax' when sent via route.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   HTML wrapped in JSON formatted commands.
   */
  public function predictionsCallbackHtml($nojs) {
    $response = new AjaxResponse();
    try {
      $json = \Drupal::request()->getContent();
      if (!empty($json)) {
        if ($predictions = json_decode($json)) {
          $build = [
            '#theme' => 'mobilenetjs_example_caption',
            '#predictions' => $predictions,
          ];
          $html = \Drupal::service('renderer')->render($build);
          $response->addCommand(new HtmlCommand('figure', $html));
          return $response;
        }
        else {
          $html = '<p>JSON content is empty</p>';
        }
      }
      else {
        $html = '<p>Request content is empty</p>';
      }
    }
    catch (Exception $exc) {
      $html = '<p>' . $exc->getMessage() . '</p>';
    }
    $response->addCommand(new HtmlCommand('.info', $html));
    return $response;
  }

  /**
   * Respond to an AJAX POST request containing JSON content.
   *
   * @param string $nojs
   *   String ='ajax' when sent via route.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON formatted content.
   */
  public function predictionsCallbackJson($nojs) {
    $response = [
      'message' => 'predictionsCallback received request',
      'data' => '',
      'status' => 0,
    ];
    try {
      $json = \Drupal::request()->getContent();
      if (!empty($json)) {
        if ($content = json_decode($json)) {
          $response['data'] = $content;
          $response['status'] = 1;
        }
        else {
          $response['error'] = 'JSON content is empty';
        }
      }
      else {
        $response['error'] = 'Request content is empty';
      }
    }
    catch (Exception $exc) {
      echo $exc->getTraceAsString();
      $response['error'] = $exc->getMessage();
    }
    return new JsonResponse($response);
  }

}
