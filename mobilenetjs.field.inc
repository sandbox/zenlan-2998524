<?php

/**
 * @file
 * Implement a mobilenetjs field, based on the file module's file field.
 */

/**
 * Prepares variables for mobilenetjs/image formatter templates.
 *
 * Default template: mobilenetjs-formatter.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - item_attributes: An optional associative array of html attributes to be
 *     placed in the img tag.
 *   - image_style: An optional image style.
 *   - url: An optional \Drupal\Core\Url object.
 */
function template_preprocess_mobilenetjs_formatter(array &$variables) {

  if (!function_exists('template_preprocess_image_formatter')) {
    module_load_include('inc', 'image', 'image.field');
  }
  template_preprocess_image_formatter($variables);

}
