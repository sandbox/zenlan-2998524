<?php

namespace Drupal\mobilenetjs\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Class MobilenetjsController.
 */
class MobilenetjsController extends ControllerBase {

  /**
   * Respond to an AJAX POST request containing JSON content.
   *
   * @param string $nojs
   *   String ='ajax' when sent via route.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   HTML wrapped in JSON formatted commands.
   */
  public function mobilenetjsCallback($nojs) {
    $response = new AjaxResponse();
    try {
      $json = \Drupal::request()->getContent();
      if (!empty($json)) {
        if ($predictions = json_decode($json)) {
          $build = [
            '#theme' => 'mobilenetjs_caption',
            '#predictions' => $predictions,
          ];
          $html = \Drupal::service('renderer')->render($build);
          $response->addCommand(new HtmlCommand('figcaption', $html));
          return $response;
        }
        else {
          $html = '<p>JSON content is empty</p>';
        }
      }
      else {
        $html = '<p>Request content is empty</p>';
      }
    }
    catch (Exception $exc) {
      $html = '<p>' . $exc->getMessage() . '</p>';
    }
    return $response;
  }

}
