<?php

namespace Drupal\mobilenetjs\Plugin\Field\FieldFormatter;

use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'mobilenetjs' formatter.
 *
 * @FieldFormatter(
 *   id = "mobilenetjs",
 *   label = @Translation("MobilenetJS"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class MobilenetjsFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'attach' => ['mobilenetjs/mobilenetjs.default'],
      'mobilenetjs' => [
        'pathConnect' => Url::fromRoute('mobilenetjs.predictions', [])->toString(),
      ],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $attach = $this->getSetting('attach');
    $settings = $this->getSetting('mobilenetjs');
    foreach ($elements as $delta => &$element) {
      $element['#theme'] = 'mobilenetjs_formatter';
      $element['#attached']['library'] = array_merge([
        'mobilenetjs/mobilenetjs',
        'mobilenetjs/tensorflow',
        'mobilenetjs/tensorflow.mobilenet',
      ], $attach);
      $element['#attached']['drupalSettings']['mobilenetjs'] = $settings;
    }
    return $elements;
  }

}
