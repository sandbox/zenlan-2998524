function initMobilenetJS(properties) {
  console.log('MobilenetJS default script loaded');
  new MJS_default({
    'connector' : new MJS_connector(properties)
  });
}

class MJS_default {

  constructor(properties) {
    this.loading = false;
    this.properties = properties;
    this.ui = new MJSUI_default({
      onPredict: this.onPredict.bind(this)
    });
  }

  async onPredict() {
    if (this.loading) return;
    this.loading = true;
    console.log('onPredict begin');
    this.ui.showCaption('Loading, please wait');
    try {
      if (!this.ui.image) {
        throw new Error('Image load failed');
      }
      const result = await this.properties.connector.fetchPredictions(this.ui.image);
      console.log('onPredict fetched', result);
      const json = await this.properties.connector.postPredictions(result);
      console.log('onPredict posted', json);
      json.forEach(command => {
        console.log('onPredict command:', command);
        if (command.selector == 'figcaption') {
          this.ui.showCaption(command.data);
        }
      });
    } catch (err) {
      console.log('onPredict', err);
      this.ui.showCaption(err);
    } finally {
      this.loading = false;
      console.log('onPredict final');
    }
  }

}

class MJSUI_default {

  constructor(properties) {
    this.properties = properties;
    this._onPredict = this._onPredict.bind(this);
    this.buttons = [...document.querySelectorAll('.mobilenetjs button')];
    this.buttons.forEach(button => button.addEventListener('click', this._onPredict));
  }

  _onPredict(e) {
    e.preventDefault();
    const elem = e.srcElement.previousElementSibling;
    this.image = elem.querySelector('img');
    this.caption = elem.querySelector('figcaption');
    this.properties.onPredict();
  }

  showCaption(caption) {
    this.caption.innerHTML = caption;
  }

}

