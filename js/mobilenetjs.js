/**
 * @file
 * Contains js for the mobilenet_js library.
 */

(function (Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.MobilenetJS = {
    attach: function (context, settings) {
      console.log('MobilenetJS:attach');
      let properties = settings.mobilenetjs;
      properties.urlPost = window.location.protocol + '//'
              + window.location.host
              + settings.mobilenetjs.pathConnect + '/ajax';
      initMobilenetJS(properties);
    }
  };

})(Drupal, drupalSettings);

class MJS_connector {

    constructor(properties) {
      this.properties = properties;
    }

    async postPredictions(data, callback) {
      console.log('postPredictions begin');
      let result = {};
      try {
        const res = await fetch(this.properties.urlPost, {
          method: 'POST',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          }
        });
        console.log('postPredictions fetched');
        console.log(res);
        if (res.status >= 400) {
          throw new Error('Predictions failed ' + res.status);
        }
        result = await res.json();
        console.log('postPredictions posted');
      } catch (err) {
        console.log('postPredictions', err);
        result = {'error' : err};
      } finally {
        console.log('postPredictions final ', result);
        return result;
      }
    }

    async fetchPredictions(img) {
      console.log('fetchPredictions begin');
      let result = {};
      try {
        if (!this.model) {
          this.model = await window.mobilenet.load();
        }
        if (!this.model) {
          throw new Error('Model load failed');
        }
        result = await this.model.classify(img);
        if (!result) {
          throw new Error('Predictions failed');
        }
        console.log(result);
      } catch (err) {
        console.log('fetchPredictions', err);
        result = {'error' : err};
      } finally {
        console.log('fetchPredictions final ', result);
        return result;
      }
    }

  }
